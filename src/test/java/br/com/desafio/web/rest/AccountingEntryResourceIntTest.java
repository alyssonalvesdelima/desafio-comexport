package br.com.desafio.web.rest;

import br.com.desafio.DesafioApp;

import br.com.desafio.domain.AccountingEntry;
import br.com.desafio.repository.AccountingEntryRepository;
import br.com.desafio.service.AccountingEntryService;
import br.com.desafio.service.dto.AccountingEntryDTO;
import br.com.desafio.service.mapper.AccountingEntryMapper;
import br.com.desafio.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Validator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static br.com.desafio.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Accounting_entryResource REST controller.
 *
 * @see AccountingEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DesafioApp.class)
public class AccountingEntryResourceIntTest {

    private static final Long DEFAULT_ACCOUNTING_ACCOUNT = 1L;
    private static final Long UPDATED_ACCOUNTING_ACCOUNT = 2L;

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(2);

    @Autowired
    private AccountingEntryRepository accounting_entryRepository;

    @Autowired
    private AccountingEntryMapper accounting_entryMapper;

    @Autowired
    private AccountingEntryService accounting_entryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private Validator validator;

    private MockMvc restAccounting_entryMockMvc;

    private AccountingEntry accounting_entry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AccountingEntryResource accounting_entryResource = new AccountingEntryResource(accounting_entryService);
        this.restAccounting_entryMockMvc = MockMvcBuilders.standaloneSetup(accounting_entryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AccountingEntry createEntity() {
        AccountingEntry accounting_entry = new AccountingEntry()
            .accounting_account(DEFAULT_ACCOUNTING_ACCOUNT)
            .date(DEFAULT_DATE)
            .value(DEFAULT_VALUE);
        return accounting_entry;
    }

    @Before
    public void initTest() {
        accounting_entryRepository.deleteAll();
        accounting_entry = createEntity();
    }

    @Test
    public void createAccounting_entry() throws Exception {
        int databaseSizeBeforeCreate = accounting_entryRepository.findAll().size();

        // Create the Accounting_entry
        AccountingEntryDTO accounting_entryDTO = accounting_entryMapper.toDto(accounting_entry);
        restAccounting_entryMockMvc.perform(post("/api/accounting-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accounting_entryDTO)))
            .andExpect(status().isCreated());

        // Validate the Accounting_entry in the database
        List<AccountingEntry> accounting_entryList = accounting_entryRepository.findAll();
        assertThat(accounting_entryList).hasSize(databaseSizeBeforeCreate + 1);
        AccountingEntry testAccounting_entry = accounting_entryList.get(accounting_entryList.size() - 1);
        assertThat(testAccounting_entry.getAccounting_account()).isEqualTo(DEFAULT_ACCOUNTING_ACCOUNT);
        assertThat(testAccounting_entry.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testAccounting_entry.getValue()).isEqualTo(DEFAULT_VALUE);
    }

    @Test
    public void createAccounting_entryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = accounting_entryRepository.findAll().size();

        // Create the Accounting_entry with an existing ID
        accounting_entry.setId("existing_id");
        AccountingEntryDTO accounting_entryDTO = accounting_entryMapper.toDto(accounting_entry);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAccounting_entryMockMvc.perform(post("/api/accounting-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accounting_entryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Accounting_entry in the database
        List<AccountingEntry> accounting_entryList = accounting_entryRepository.findAll();
        assertThat(accounting_entryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllAccounting_entries() throws Exception {
        // Initialize the database
        accounting_entryRepository.save(accounting_entry);

        // Get all the accounting_entryList
        restAccounting_entryMockMvc.perform(get("/api/accounting-entries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(accounting_entry.getId())))
            .andExpect(jsonPath("$.[*].accounting_account").value(hasItem(DEFAULT_ACCOUNTING_ACCOUNT.intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())));
    }
    
    @Test
    public void getAccounting_entry() throws Exception {
        // Initialize the database
        accounting_entryRepository.save(accounting_entry);

        // Get the accounting_entry
        restAccounting_entryMockMvc.perform(get("/api/accounting-entries/{id}", accounting_entry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(accounting_entry.getId()))
            .andExpect(jsonPath("$.accounting_account").value(DEFAULT_ACCOUNTING_ACCOUNT.intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()));
    }

    @Test
    public void getNonExistingAccounting_entry() throws Exception {
        // Get the accounting_entry
        restAccounting_entryMockMvc.perform(get("/api/accounting-entries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateAccounting_entry() throws Exception {
        // Initialize the database
        accounting_entryRepository.save(accounting_entry);

        int databaseSizeBeforeUpdate = accounting_entryRepository.findAll().size();

        // Update the accounting_entry
        AccountingEntry updatedAccounting_entry = accounting_entryRepository.findById(accounting_entry.getId()).get();
        updatedAccounting_entry
            .accounting_account(UPDATED_ACCOUNTING_ACCOUNT)
            .date(UPDATED_DATE)
            .value(UPDATED_VALUE);
        AccountingEntryDTO accounting_entryDTO = accounting_entryMapper.toDto(updatedAccounting_entry);

        restAccounting_entryMockMvc.perform(put("/api/accounting-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accounting_entryDTO)))
            .andExpect(status().isOk());

        // Validate the Accounting_entry in the database
        List<AccountingEntry> accounting_entryList = accounting_entryRepository.findAll();
        assertThat(accounting_entryList).hasSize(databaseSizeBeforeUpdate);
        AccountingEntry testAccounting_entry = accounting_entryList.get(accounting_entryList.size() - 1);
        assertThat(testAccounting_entry.getAccounting_account()).isEqualTo(UPDATED_ACCOUNTING_ACCOUNT);
        assertThat(testAccounting_entry.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testAccounting_entry.getValue()).isEqualTo(UPDATED_VALUE);
    }

    @Test
    public void updateNonExistingAccounting_entry() throws Exception {
        int databaseSizeBeforeUpdate = accounting_entryRepository.findAll().size();

        // Create the Accounting_entry
        AccountingEntryDTO accounting_entryDTO = accounting_entryMapper.toDto(accounting_entry);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAccounting_entryMockMvc.perform(put("/api/accounting-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(accounting_entryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Accounting_entry in the database
        List<AccountingEntry> accounting_entryList = accounting_entryRepository.findAll();
        assertThat(accounting_entryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteAccounting_entry() throws Exception {
        // Initialize the database
        accounting_entryRepository.save(accounting_entry);

        int databaseSizeBeforeDelete = accounting_entryRepository.findAll().size();

        // Get the accounting_entry
        restAccounting_entryMockMvc.perform(delete("/api/accounting-entries/{id}", accounting_entry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AccountingEntry> accounting_entryList = accounting_entryRepository.findAll();
        assertThat(accounting_entryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountingEntry.class);
        AccountingEntry accounting_entry1 = new AccountingEntry();
        accounting_entry1.setId("id1");
        AccountingEntry accounting_entry2 = new AccountingEntry();
        accounting_entry2.setId(accounting_entry1.getId());
        assertThat(accounting_entry1).isEqualTo(accounting_entry2);
        accounting_entry2.setId("id2");
        assertThat(accounting_entry1).isNotEqualTo(accounting_entry2);
        accounting_entry1.setId(null);
        assertThat(accounting_entry1).isNotEqualTo(accounting_entry2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AccountingEntryDTO.class);
        AccountingEntryDTO accounting_entryDTO1 = new AccountingEntryDTO();
        accounting_entryDTO1.setId("id1");
        AccountingEntryDTO accounting_entryDTO2 = new AccountingEntryDTO();
        assertThat(accounting_entryDTO1).isNotEqualTo(accounting_entryDTO2);
        accounting_entryDTO2.setId(accounting_entryDTO1.getId());
        assertThat(accounting_entryDTO1).isEqualTo(accounting_entryDTO2);
        accounting_entryDTO2.setId("id2");
        assertThat(accounting_entryDTO1).isNotEqualTo(accounting_entryDTO2);
        accounting_entryDTO1.setId(null);
        assertThat(accounting_entryDTO1).isNotEqualTo(accounting_entryDTO2);
    }
}
