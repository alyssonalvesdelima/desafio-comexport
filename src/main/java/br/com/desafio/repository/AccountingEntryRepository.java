package br.com.desafio.repository;

import br.com.desafio.domain.AccountingEntry;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data MongoDB repository for the Accounting_entry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AccountingEntryRepository extends MongoRepository<AccountingEntry, String> {

}
