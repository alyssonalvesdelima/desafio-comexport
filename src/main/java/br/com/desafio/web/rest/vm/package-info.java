/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.desafio.web.rest.vm;
