package br.com.desafio.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.com.desafio.service.AccountingEntryService;
import br.com.desafio.web.rest.errors.BadRequestAlertException;
import br.com.desafio.web.rest.util.HeaderUtil;
import br.com.desafio.service.dto.AccountingEntryDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Accounting_entry.
 */
@RestController
@RequestMapping("/api")
public class AccountingEntryResource {

    private final Logger log = LoggerFactory.getLogger(AccountingEntryResource.class);

    private static final String ENTITY_NAME = "desafioAccountingEntry";

    private final AccountingEntryService accounting_entryService;

    public AccountingEntryResource(AccountingEntryService accounting_entryService) {
        this.accounting_entryService = accounting_entryService;
    }

    /**
     * POST  /accounting-entries : Create a new accounting_entry.
     *
     * @param accounting_entryDTO the accounting_entryDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accounting_entryDTO, or with status 400 (Bad Request) if the accounting_entry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/accounting-entries")
    @Timed
    public ResponseEntity<AccountingEntryDTO> createAccounting_entry(@RequestBody AccountingEntryDTO accounting_entryDTO) throws URISyntaxException {
        log.debug("REST request to save Accounting_entry : {}", accounting_entryDTO);
        if (accounting_entryDTO.getId() != null) {
            throw new BadRequestAlertException("A new accounting_entry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AccountingEntryDTO result = accounting_entryService.save(accounting_entryDTO);
        return ResponseEntity.created(new URI("/api/accounting-entries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /accounting-entries : Updates an existing accounting_entry.
     *
     * @param accounting_entryDTO the accounting_entryDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accounting_entryDTO,
     * or with status 400 (Bad Request) if the accounting_entryDTO is not valid,
     * or with status 500 (Internal Server Error) if the accounting_entryDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/accounting-entries")
    @Timed
    public ResponseEntity<AccountingEntryDTO> updateAccounting_entry(@RequestBody AccountingEntryDTO accounting_entryDTO) throws URISyntaxException {
        log.debug("REST request to update Accounting_entry : {}", accounting_entryDTO);
        if (accounting_entryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AccountingEntryDTO result = accounting_entryService.save(accounting_entryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, accounting_entryDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /accounting-entries : get all the accounting_entries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of accounting_entries in body
     */
    @GetMapping("/accounting-entries")
    @Timed
    public List<AccountingEntryDTO> getAllAccounting_entries() {
        log.debug("REST request to get all Accounting_entries");
        return accounting_entryService.findAll();
    }

    /**
     * GET  /accounting-entries/:id : get the "id" accounting_entry.
     *
     * @param id the id of the accounting_entryDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accounting_entryDTO, or with status 404 (Not Found)
     */
    @GetMapping("/accounting-entries/{id}")
    @Timed
    public ResponseEntity<AccountingEntryDTO> getAccounting_entry(@PathVariable String id) {
        log.debug("REST request to get Accounting_entry : {}", id);
        Optional<AccountingEntryDTO> accounting_entryDTO = accounting_entryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(accounting_entryDTO);
    }

    /**
     * DELETE  /accounting-entries/:id : delete the "id" accounting_entry.
     *
     * @param id the id of the accounting_entryDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/accounting-entries/{id}")
    @Timed
    public ResponseEntity<Void> deleteAccounting_entry(@PathVariable String id) {
        log.debug("REST request to delete Accounting_entry : {}", id);
        accounting_entryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
