package br.com.desafio.service.impl;

import br.com.desafio.service.AccountingEntryService;
import br.com.desafio.domain.AccountingEntry;
import br.com.desafio.repository.AccountingEntryRepository;
import br.com.desafio.service.dto.AccountingEntryDTO;
import br.com.desafio.service.mapper.AccountingEntryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Accounting_entry.
 */
@Service
public class AccountingEntryServiceImpl implements AccountingEntryService {

    private final Logger log = LoggerFactory.getLogger(AccountingEntryServiceImpl.class);

    private final AccountingEntryRepository accounting_entryRepository;

    private final AccountingEntryMapper accounting_entryMapper;

    public AccountingEntryServiceImpl(AccountingEntryRepository accounting_entryRepository, AccountingEntryMapper accounting_entryMapper) {
        this.accounting_entryRepository = accounting_entryRepository;
        this.accounting_entryMapper = accounting_entryMapper;
    }

    /**
     * Save a accounting_entry.
     *
     * @param accounting_entryDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public AccountingEntryDTO save(AccountingEntryDTO accounting_entryDTO) {
        log.debug("Request to save Accounting_entry : {}", accounting_entryDTO);

        AccountingEntry accounting_entry = accounting_entryMapper.toEntity(accounting_entryDTO);
        accounting_entry = accounting_entryRepository.save(accounting_entry);
        return accounting_entryMapper.toDto(accounting_entry);
    }

    /**
     * Get all the accounting_entries.
     *
     * @return the list of entities
     */
    @Override
    public List<AccountingEntryDTO> findAll() {
        log.debug("Request to get all Accounting_entries");
        return accounting_entryRepository.findAll().stream()
            .map(accounting_entryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one accounting_entry by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Optional<AccountingEntryDTO> findOne(String id) {
        log.debug("Request to get Accounting_entry : {}", id);
        return accounting_entryRepository.findById(id)
            .map(accounting_entryMapper::toDto);
    }

    /**
     * Delete the accounting_entry by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Accounting_entry : {}", id);
        accounting_entryRepository.deleteById(id);
    }
}
