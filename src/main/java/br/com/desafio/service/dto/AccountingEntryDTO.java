package br.com.desafio.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the Accounting_entry entity.
 */
public class AccountingEntryDTO implements Serializable {

    private String id;

    private Long accounting_account;

    private LocalDate date;

    private BigDecimal value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccounting_account() {
        return accounting_account;
    }

    public void setAccounting_account(Long accounting_account) {
        this.accounting_account = accounting_account;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountingEntryDTO accounting_entryDTO = (AccountingEntryDTO) o;
        if (accounting_entryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accounting_entryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Accounting_entryDTO{" +
            "id=" + getId() +
            ", accounting_account=" + getAccounting_account() +
            ", date='" + getDate() + "'" +
            ", value=" + getValue() +
            "}";
    }
}
