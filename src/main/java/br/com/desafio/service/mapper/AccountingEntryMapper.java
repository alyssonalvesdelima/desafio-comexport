package br.com.desafio.service.mapper;

import br.com.desafio.domain.*;
import br.com.desafio.service.dto.AccountingEntryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Accounting_entry and its DTO Accounting_entryDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AccountingEntryMapper extends EntityMapper<AccountingEntryDTO, AccountingEntry> {



    default AccountingEntry fromId(String id) {
        if (id == null) {
            return null;
        }
        AccountingEntry accounting_entry = new AccountingEntry();
        accounting_entry.setId(id);
        return accounting_entry;
    }
}
