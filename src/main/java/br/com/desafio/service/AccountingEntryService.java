package br.com.desafio.service;

import br.com.desafio.service.dto.AccountingEntryDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Accounting_entry.
 */
public interface AccountingEntryService {

    /**
     * Save a accounting_entry.
     *
     * @param accounting_entryDTO the entity to save
     * @return the persisted entity
     */
    AccountingEntryDTO save(AccountingEntryDTO accounting_entryDTO);

    /**
     * Get all the accounting_entries.
     *
     * @return the list of entities
     */
    List<AccountingEntryDTO> findAll();


    /**
     * Get the "id" accounting_entry.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AccountingEntryDTO> findOne(String id);

    /**
     * Delete the "id" accounting_entry.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
