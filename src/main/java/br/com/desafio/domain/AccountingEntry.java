package br.com.desafio.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Accounting_entry.
 */
@Document(collection = "accounting_entry")
public class AccountingEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("accounting_account")
    private Long accounting_account;

    @Field("date")
    private LocalDate date;

    @Field("value")
    private BigDecimal value;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccounting_account() {
        return accounting_account;
    }

    public AccountingEntry accounting_account(Long accounting_account) {
        this.accounting_account = accounting_account;
        return this;
    }

    public void setAccounting_account(Long accounting_account) {
        this.accounting_account = accounting_account;
    }

    public LocalDate getDate() {
        return date;
    }

    public AccountingEntry date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public AccountingEntry value(BigDecimal value) {
        this.value = value;
        return this;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountingEntry accounting_entry = (AccountingEntry) o;
        if (accounting_entry.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), accounting_entry.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Accounting_entry{" +
            "id=" + getId() +
            ", accounting_account=" + getAccounting_account() +
            ", date='" + getDate() + "'" +
            ", value=" + getValue() +
            "}";
    }
}
